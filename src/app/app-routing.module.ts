import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SensorsListComponent } from './components/sensors-list/sensors-list.component';
import { SensorDetailsComponent } from './components/sensor-details/sensor-details.component';


const routes: Routes = [
  { path: '', component: SensorsListComponent },
  { path: 'sensors/list', component: SensorsListComponent },
  { path: 'sensor/:key', component: SensorDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

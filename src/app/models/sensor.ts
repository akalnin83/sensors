export class Sensor {
    key: string;
    description: string;
    value: number;
    time: Date;
}
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { SensorsListComponent } from './components/sensors-list/sensors-list.component';
import { SensorDetailsComponent } from './components/sensor-details/sensor-details.component';

@NgModule({
  declarations: [
    AppComponent,
    SensorsListComponent,
    SensorDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SensorLite } from '../models/sensor-lite';
import { Sensor } from '../models/sensor';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getSensorDetails(key: string): Observable<Sensor> {
    return of({
      key: key,
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      value: this.getRandomSensorValue(),
      time: new Date()
    });
  }

  getSensorsList(): Observable<SensorLite[]> {
    return of(this.getStubList());
  }

  private getStubList(): SensorLite[] {
    const result = [];
    for (let i = 1; i < 5001; i++) {
      result.push({
        key: 'Sensor-' + i,
        value: this.getRandomSensorValue()
      })
    }

    return result;
  }

  private getRandomSensorValue() {
    return Math.floor(Math.random() * (100000)) - 50000;
  }
}

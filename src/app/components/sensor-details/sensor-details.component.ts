import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Sensor } from 'src/app/models/sensor';
import { ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-sensor-details',
  templateUrl: './sensor-details.component.html',
  styleUrls: ['./sensor-details.component.css']
})
export class SensorDetailsComponent implements OnInit {

  sensor: Sensor;

  constructor(private readonly _dataService: DataService
    , private readonly _route: ActivatedRoute) { }

  ngOnInit() {
    const currentKey = this._route.snapshot.params.key; 
    this.load(currentKey);
  }

  private load(key: string) {
    this._dataService.getSensorDetails(key).pipe(
      first()
    ).subscribe(res => this.sensor = res);
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { SensorLite } from 'src/app/models/sensor-lite';
import { first, takeUntil } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
import { timer, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-sensors-list',
  templateUrl: './sensors-list.component.html',
  styleUrls: ['./sensors-list.component.css']
})
export class SensorsListComponent implements OnInit, OnDestroy {

  private $destroy = new Subject<void>();
  private timerSubscription: Subscription;
  manualRefreshMode = false;

  rows: SensorLite[][];

  constructor(private readonly _dataService:  DataService) { }

  ngOnInit() {
    this.startAutoRefresh();
  }

  ngOnDestroy() {
    this.$destroy.next();
    this.$destroy.complete();
  }

  switchMode() {
    if (this.manualRefreshMode) {
      this.startAutoRefresh();
    } else {
      this.timerSubscription.unsubscribe();
      this.timerSubscription = null;
    }
  }

  refresh() {
    this.loadData();
  }

  private startAutoRefresh() {
    this.timerSubscription = timer(0, 1000).pipe(
      takeUntil(this.$destroy)
    ).subscribe(() => this.loadData());
  }

  private loadData() {
    this._dataService.getSensorsList().pipe(
      first()
    ).subscribe(res => this.mapRows(res));
  }

  private mapRows(sensors: SensorLite[]) {
    this.rows = [];
    const rowsNumber = Math.ceil(sensors.length / 6);
    for (let i = 0; i < rowsNumber; i++) {
      this.rows.push(sensors.splice(0, 6));
    }
  }

}
